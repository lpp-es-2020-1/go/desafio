



1.Desafio GO vs Rust

Construtor de Árvore

Faça com que a função TreeConstructor (strArr) pegue a matriz de strings armazenadas em strArr, que conterá pares de inteiros no seguinte formato: (i1, i2), onde i1 representa um nó filho em uma árvore e o segundo inteiro i2 significa que ele é o pai de i1. Por exemplo: se strArr for ["(1,2)", "(2,4)", "(7,2)"], isso formará a seguinte árvore:

4
/
2
/\
1 7

que você pode ver, forma uma árvore binária apropriada. O programa deve, neste caso, retornar a string true porque uma árvore binária válida pode ser formada. Se uma árvore binária adequada não puder ser formada com os pares inteiros, retorne a string false. Todos os inteiros da árvore serão únicos, o que significa que só pode haver um nó na árvore com o valor inteiro fornecido.

Exemplos:

Entrada: []string {"(1,2)", "(2,4)", "(5,7)", "(7,2)", "(9,5)"}
Saida: true

Entrada: []string {"(1,2)", "(3,2)", "(2,12)", "(5,2)"}
Saida: false